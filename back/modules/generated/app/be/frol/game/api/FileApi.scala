package be.frol.game.api


@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"), date = "2021-02-08T18:07:22.741Z[Etc/UTC]")
trait FileApi {
  /**
    * get a file
    * get a file
    */
  def getFile(id: String): Unit
}
